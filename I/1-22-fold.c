#include <stdio.h>

#define MAX_LINE 80
#define TRUE 1
#define FALSE 0

void printBufferFromTo(int buffer[], int from, int to);

int main()
{
    int c;
    int curPos = 0;
    int lastBlank = -1;
    int buffer[MAX_LINE];
    int afterMaxLine = FALSE;

    while ((c = getchar()) != EOF) {
        if (c  == '\n') {
            printBufferFromTo(buffer, 0, curPos);
            putchar(c);
            curPos = 0;
            lastBlank = -1;
            afterMaxLine = FALSE;
        }
        else if (curPos < MAX_LINE) {
            buffer[curPos] = c;
            ++curPos;
            if (c == ' ' && afterMaxLine == FALSE) {
                lastBlank = curPos;
            }
            else if (c == ' ') {
                printBufferFromTo(buffer, 0, curPos);
                putchar('\n');
                curPos = 0;
                lastBlank = -1;
                afterMaxLine = FALSE;
            }
        }
        else if (lastBlank > -1) {
            printBufferFromTo(buffer, 0, lastBlank);
            putchar('\n');
            printBufferFromTo(buffer, lastBlank, curPos + 1);
            curPos = 0;
            lastBlank = -1;
            afterMaxLine = FALSE;
        }
        else {
            printBufferFromTo(buffer, 0, MAX_LINE);
            curPos = 0;
            afterMaxLine = TRUE;
        }
    }
    
    return 0;
}

void printBufferFromTo(int buffer[], int from, int to) {
    for(int i = from; i < to; ++i) {
        putchar(buffer[i]);
    }
}
