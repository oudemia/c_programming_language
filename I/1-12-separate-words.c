#include <stdio.h>

#define TRUE 1
#define FALSE 0

int main()
{
    int c, insideWord;

    while ((c = getchar()) != EOF) {
        if (c == '\n' || c == '\t' || c == ' ') {
            if (insideWord == TRUE) { 
                putchar('\n');
                insideWord = FALSE;       
            }
        } else {
            if (insideWord == FALSE)
                insideWord = TRUE;
            putchar(c);
        }
    }
    return 0;
}
