#include <stdio.h>

main() {
    float fahrVal, celVal;
    int min, max, step;

    min = 0;
    max = 200;
    step = 20;

    printf("Temperatures in degree Fahrenheit with their equivalent in degree Celsius:\n\n");
    
    fahrVal = min;
    while(fahrVal <= max) {
        celVal = (5.0/9.0) * (fahrVal - 32.0);
        printf("%3.0f\t%3.1f\n", fahrVal, celVal);
        fahrVal += step;
    }
}
