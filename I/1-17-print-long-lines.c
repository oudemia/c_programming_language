/* I am assuming that at this learning stage,
 * we may assume a maximum amount of relevant lines,
 * as we also labeled a maximum line lenght in the book
 * */

#include <stdio.h>

#define MAXLINE 1000
#define MAX_AMOUNT 1000
#define MIN_LENGHT 80

int getLine(char line[], int max);
void copy(char from[], char to[]);

int main()
{
    int len;
    char current[MAXLINE];
    char longest[MAX_AMOUNT][MAXLINE];

    int i = 0;
    while ((len = getLine(current, MAXLINE)) > 0 && i < MAX_AMOUNT) {
        if (len > MIN_LENGHT) {
            copy(current, longest[i]);
            ++i;
        }
    }

    if (i > 0) {
        printf("\n\nThese are the input lines longer than %d characters:\n", MIN_LENGHT);
        for (int j = 0; j < i; j++)
            printf("\n%s\n", longest[j]);
    }
    else
        printf("\n\nThere were no lines longer than %d characters.\n", MIN_LENGHT);

    return 0;
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        i++;
    }
    line[i] = '\0';
    return i;
}

void copy(char from[], char to[])
{
    int i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}
