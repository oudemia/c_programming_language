#include <stdio.h>

main() {
    float fahrVal;
    int celVal, min, max, step;

    min = 0;
    max = 300;
    step = 20;

    printf("And because the second table was so much fun, here ist is again, but in reverse:\n\n");
    for(celVal = max; celVal >= min; celVal -= step) {
        fahrVal = ( celVal * 9.0 / 5.0 ) + 32.0;
        printf("%3d\t%3.1f\n", celVal, fahrVal);
    }
}
