/* I am ignoring words longer than 50 characters.
 * If someone really types one of these,
 * they willfully try to hurt this poor little program.
*/

#include <stdio.h>

#define TRUE 1
#define FALSE 0

#define MAXLEN 50

int max(int array[]);
int numberOfDigits(int number);
void printNTimes(char c, int n);

int main()
{
    int c;
    int insideWord = FALSE;
    int wordlen = 0;

    int usage[MAXLEN + 1] = {0};

    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\n' || c == '\t' ) {
            if (insideWord == TRUE) {
                insideWord = FALSE;

                if (wordlen <= MAXLEN)
                    ++usage[wordlen];
                wordlen = 0;
            }
        } else {
            if (insideWord == FALSE)
                insideWord = TRUE;
            ++wordlen;
        }
    }


    int maxUsed = max(usage);
    int maxDigits = numberOfDigits(maxUsed);
    int maxLenDigits = numberOfDigits(MAXLEN);
    int i;

    // printing y axis label
    printf("\n\n\nused\nn times\n");
    printNTimes(' ', maxDigits + 1);
    printf("▲\n");

    // printing the y axis and the bars
    for (i = maxUsed; i > 0; i--) {
        // y axis
        if (i % 5 == 0) {
            printf("%d", i);
            printNTimes(' ', maxDigits - numberOfDigits(i) + 1);
        }
        else {
            printNTimes(' ', maxDigits + 1);
        }
        putchar('|');

        // bars
        for ( int j = 0; j < MAXLEN + 1; j++) {
            if (usage[j] >= i) {
                printf("█");
                printNTimes(' ', maxLenDigits);
            }
            else {
                printNTimes(' ', maxLenDigits +1);
            }
        }
        printf("\n");
    }

    // printing the x axis
    printNTimes(' ', maxDigits + 1);
    for (i = 0; i < MAXLEN + 1; i++) {
        printf("---");
    }
    printf("►   word lenght\n");

    // printing x axis labels
    printNTimes(' ', maxDigits + 1);
    for (i = 0; i < MAXLEN + 1; i++) {
        if (i % 5 == 0 || usage[i] > 0) {
            int digits = numberOfDigits(i);
            printf("%d", i);
            printNTimes(' ', maxLenDigits - digits + 1);
        }
        else {
            printNTimes(' ', maxLenDigits + 1);
        }
    }
    putchar('\n');
    return 0;
}

int max(int array[])
{
    int max = 0;

    for(int i = 0; i < MAXLEN; i++) {
        if ( max < array[i] ) {
            max = array[i];
        }
    }
    return max;
}

int numberOfDigits(int number)
{
    int digits = 0;

    while ( number >= 1 ) {
        number /= 10;
        ++digits;
    }

    return digits;
}

void printNTimes(char c, int n)
{
    for (int i = 0; i < n; i++)
        putchar(c);
}
