/* I am assuming that at this learning stage,
 * we may assume a maximum amount of relevant lines,
 * as we also labeled a maximum line lenght in the book
 * */

#include <stdio.h>

#define MAXLINE 1000
#define MAX_AMOUNT 1000
#define TRUE 1
#define FALSE 0

int getLine(char line[], int max);
int firstTrailing(char line[]);
void copy(char from[], char to[], int firstTrail);

int main()
{
    int len, firstTrail;
    char current[MAXLINE];
    char nonEmpty[MAX_AMOUNT][MAXLINE];

    int i = 0;
    while ((len = getLine(current, MAXLINE)) > 0 && i < MAX_AMOUNT) {
        firstTrail = firstTrailing(current);
        if (firstTrail != 0) {
            copy(current, nonEmpty[i], firstTrail);
            ++i;    
        }
    }

    if (i > 0) {
        printf("\n\nInput without trailing whitespace:\n");
        for (int j = 0; j < i; j++)
            printf("%s\n", nonEmpty[j]);
    }
    else
        printf("\n\nThere were no non empty lines.\n");

    return 0;
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        i++;
    }
    line[i] = '\0';
    return i;
}

int firstTrailing(char line[])
{
    int afterWhite = FALSE;
    int firstTrail = -1;
    
    for (int i = 0; line[i] != '\0'; i++) {
        if (line[i] == ' ' || line[i] == '\t' || line[i] == '\n') {
            if (!afterWhite) {
                afterWhite = TRUE;
                firstTrail = i;
            }
        }
        else {
            afterWhite = FALSE;
            firstTrail = -1;
        }
    }
    return firstTrail;
}

void copy(char from[], char to[], int firstTrail)
{
    int i = 0;
    while ((to[i] = from[i]) != '\0' && i < firstTrail)
        ++i;
    to[i] = '\0';
}
