#include <stdio.h>

main() {
    float fahrVal, celVal;
    int min, max, step;

    printf("Temperatures in degree Celsius with their equivalent in degree Fahrenheit:\n\n");

    min = 0;
    max = 200;
    step = 20;

    celVal = min;
    while(celVal <= max) {
        fahrVal = (celVal *  9.0 / 5.0) + 32.0;
        printf("%3.0f\t%3.1f\n", celVal, fahrVal);
        celVal += step;
    }
}
