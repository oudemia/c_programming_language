/* In order to avoid accidentally using a tab instead of a blank in between two
 * words if either a tab or a blanc would result in desired spacing, I will use
 * a blank on these occasions.
*/

#include <stdio.h>

#define TAB_SIZE 4

void putEnChars(int n, char c);

int main()
{
    int c;
    int curPos = 0;
    int blankCount = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            ++blankCount;
        }
        else if (c == '\n') {
            putchar(c);
            curPos = 0;
            blankCount = 0;
        }
        else {
            if (blankCount > 1) {
                if ((curPos % TAB_SIZE) == 0) {
                    int tabCount = blankCount / TAB_SIZE;
                    putEnChars(tabCount, '\t');
                    curPos += tabCount;
                }
                else {
                    putEnChars(blankCount, ' ');
                    curPos += blankCount;
                }
            }
            else if (blankCount == 1) {
                putchar(' ');
                ++curPos;
            }

            putchar(c);
            ++curPos;
            blankCount = 0;
        }
    }

    return 0;
}

void putEnChars(int n, char c)
{
    for (int i = 0; i < n; ++i)
        putchar(c);
}
