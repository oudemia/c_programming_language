/* I am assuming the usage of the roman alphabet,
 * therefore limiting this histogram to 26 letters.
*/

#include <stdio.h>

#define LETTERS 26
#define SPACER 2

int max(int array[]);
int numberOfDigits(int number);
void printNTimes(char c, int n);

int main()
{
    int c;
    int usageLower[LETTERS] = {0};
    int usageUpper[LETTERS] = {0};

    while ((c = getchar()) != EOF) {
        if ( c >= 'a' && c < 'z') {
            ++usageLower[c - 'a'];
        } 
        else if ( c >= 'A' && c <= 'Z' ) {
            ++usageUpper[c - 'A'];
        }
    }

    int maxes[2] = {max(usageLower), max(usageUpper)};
    int maxUsed = max(maxes);
    int maxDigits = numberOfDigits(maxUsed);
    int i;

    // printing y axis label
    printf("\n\n\nused\nn times\n");
    printNTimes(' ', maxDigits + 1);
    printf("▲\n");

    // printing the y axis and the bars
    for (i = maxUsed; i > 0; i--) {
        // y axis
        if (i % 5 == 0) {
            printf("%d", i);
            printNTimes(' ', maxDigits - numberOfDigits(i) + 1);
        }
        else {
            printNTimes(' ', maxDigits + 1);
        }
        putchar('|');

        // bars
        int j;
        for (j = 0; j < LETTERS; j++) {
            if (usageLower[j] >= i) {
                printf("█");
                printNTimes(' ', SPACER);
            }
            else {
                printNTimes(' ', SPACER + 1);
            }
        }
        for (j = 0; j < LETTERS; j++) {
            if (usageUpper[j] >= i) {
                printf("█");
                printNTimes(' ', SPACER);
            }
            else {
                printNTimes(' ', SPACER + 1);
            }
        }
        printf("\n");
    }

    // printing the x axis
    printNTimes(' ', maxDigits + 1);
    for (i = 0; i < LETTERS * 2+ 1; i++) {
        printNTimes('-', SPACER + 1);
    }
    printf("►\n");

    // printing x axis labels
    printNTimes(' ', maxDigits + SPACER);
    for (i = 0; i < LETTERS; i++) {
            putchar(i + 'a');
            printNTimes(' ', SPACER);    
    }
    for (i = 0; i < LETTERS; i++) {
        putchar(i + 'A');
        printNTimes(' ', SPACER);
    }
    putchar('\n');
    return 0;
}

int max(int array[])
{
    int max = 0;

    for(int i = 0; i < LETTERS; i++) {
        if ( max < array[i] ) {
            max = array[i];
        }
    }
    return max;
}

int numberOfDigits(int number)
{
    int digits = 0;

    while ( number >= 1 ) {
        number /= 10;
        ++digits;
    }

    return digits;
}

void printNTimes(char c, int n)
{
    for (int i = 0; i < n; i++)
        putchar(c);
}
