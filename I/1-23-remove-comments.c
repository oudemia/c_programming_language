#include <stdio.h>

#define TRUE 1
#define FALSE 0

int main()
{
    int c;
    int singleLineComment = FALSE;
    int multiLineComment = FALSE;
    int afterSlash = FALSE;
    int afterStar = FALSE;

    while ((c = getchar()) != EOF) {
        if (singleLineComment) {
            if (c == '\n')
            singleLineComment = FALSE;
        }

        else if (multiLineComment) {
            if (afterStar && c == '/')
                multiLineComment = FALSE;
            else if (c == '*')
                afterStar = TRUE;
        }

        else {
            if (afterSlash && c == '/') {
                singleLineComment = TRUE;
                afterSlash = FALSE;
            }
            else if (afterSlash && c == '*') {
                multiLineComment = TRUE;
                afterSlash = FALSE;
            }
            else if (afterSlash) {
                putchar('/');
                putchar(c);
                afterSlash = FALSE;
            }
            else if (c == '/') {
                afterSlash = TRUE;
            }
            else {
                putchar(c);
            }
        }
    }
}
