/* I am using the tab size in form of a symbolic parameter, because
 * it should remain constant throughout the whole program.
*/

#include <stdio.h>

#define TAB_SIZE 4

int main()
{
    int c;
    int curPos = 0;

    while ((c = getchar()) != EOF) {
        if (c == '\t') {
            do {
                putchar(' ');
                ++curPos;
            }
            while ((curPos % TAB_SIZE) != 0);
        }
        else if (c == '\n') {
            putchar(c);
            curPos = 0;
        }
        else {
            putchar(c);
            ++curPos;
        }
    }

    return 0;
}
