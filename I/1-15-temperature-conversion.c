#include <stdio.h>

#define CONVERSION_CONSTANT 5.0 / 9.0

float fahrToCel(float fahr);

int main()
{
    float fahrVal, celVal;
    int min, max, step;

    min = 0;
    max = 200;
    step = 20;

    printf("Temperatures in degree Fahrenheit with their equivalent in degree Celsius:\n\n");
    
    for(fahrVal = min; fahrVal <= max; fahrVal += step) {
        celVal = fahrToCel(fahrVal);
        printf("%3.0f\t%3.1f\n", fahrVal, celVal);
    }

    return 0;
}

float fahrToCel(float fahr)
{
    return CONVERSION_CONSTANT * (fahr - 32.0);
}
