#include <stdio.h>

int main()
{
    int c;
    double blanks, tabs, newlines;

    while((c = getchar()) != EOF) {
        if(c == ' ') {
            ++blanks;
        }
        if(c == '\t') {
            ++tabs;
        }
        if(c == '\n') {
            ++newlines;
        }
    }

    printf("\n\nNumber of...\n");
    printf("blanks: %.0f\ntabs: %.0f\nnewlines: %.0f\n", blanks, tabs, newlines);

    return 0;
}
