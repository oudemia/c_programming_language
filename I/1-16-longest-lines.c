#include <stdio.h>

#define MAXLINE 1000 /* still thik that's ugly, but well...*/

int getLine(char line[], int max);
void copy(char from[], char to[]);

int main()
{
    int len, max;
    char current[MAXLINE], longest[MAXLINE];

    max = 0;
    while ((len = getLine(current, MAXLINE)) > 0) {
        if (len > max) {
            max = len;
            copy(current, longest);
        }
    }
    if (max > MAXLINE)
        printf("\n\nThe longest line was actually %d characters, therefore too long to save. Here are the first %d characters:\n%s\n", max, MAXLINE, longest);
    else if (max > 0)
        printf("\n\nThe longest line contains %d characters:\n%s\n", max, longest);
    else
       printf("\n\nThere was no line to read.");

    return 0;
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        i++;
    }
    line[i] = '\0';
    return i;
}

void copy(char from[], char to[])
{
    int i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}
