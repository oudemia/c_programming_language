/* I am assuming that at this learning stage,
 * we may assume a maximum amount of relevant lines,
 * as we also labeled a maximum line lenght in the book
 * */

#include <stdio.h>

#define MAXLINE 1000
#define MAX_AMOUNT 1000
#define TRUE 1
#define FALSE 0

int getLine(char line[], int max);
void copyInReverse(char from[], char to[], int lastIndex);

int main()
{
    int lastIndex;
    char current[MAXLINE];
    char reversed[MAX_AMOUNT][MAXLINE];

    int i = 0;
    while ((lastIndex = getLine(current, MAXLINE)) > 0 && i < MAX_AMOUNT) {
        copyInReverse(current, reversed[i], lastIndex);
        ++i;
    }

    if (i > 0) {
        printf("\n\nInput reversed line by line:\n");
        for (int j = 0; j < i; j++)
            printf("%s", reversed[j]);
    }
    else
        printf("\n\nNo input provided.\n");

    return 0;
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        i++;
    }
    line[i] = '\0';
    return i;
}

void copyInReverse(char from[], char to[], int lastIndex)
{
    to[lastIndex] = '\0';

    for (int i = 0; i < lastIndex; i++) {
        to[lastIndex - 1 - i] = from[i];
    }
}
