# include <stdio.h>
# include <stdlib.h>

#define MAXLINE 1000

enum bool {FALSE, TRUE};
int getInt(int* number, int max);
unsigned rightrot(unsigned x, int n);
int powOfTen(int pow);

int main()
{
    int x, n, p, xLen, yLen, nLen, pLen;

    for (;;) {
        printf("Insert number x:\n");
        if ((xLen = getInt(&x, MAXLINE)) <= 0) {
            printf("Error: please enter a positive number as x\n");
            exit(0);
        }

        printf("By how many bit positions should x be rotated?\n");
        if ((nLen = getInt(&n, MAXLINE)) == 0) {
            printf("Error: input has to be a positive number\n");
            exit(0);
        }

        printf("result: %u\n\n", rightrot((unsigned)x, n));

    }
    return 0;
}


int getInt(int* number, int max)
{
    int c, i, j, input[max];

    *number = 0;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    return i;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}


/* return value of x rotated to the right by n positions
 */
unsigned rightrot(unsigned x, int n)
{
    // idea: move x one bit to the right
    // if 'pushed out' bit was a one, x & 10....0

    for (int i = 0; i < n; ++i) {
        enum bool x_even = (x % 2 == 0);
        x = x >> 1;
        if (!x_even) {
            x = x | ~(~0 >> 1);
        }
    }
    return x;
}
