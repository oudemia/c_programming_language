#include <stdio.h>
#include <float.h>
#include <limits.h>

int main() {
    printf("Listing the ranges of different data types on this machine:\n\n");

    printf("Maximum values:\n");
    printf("signed char: %d\n", SCHAR_MAX);
    printf("unsigned char: %d\n", CHAR_MAX);
    printf("signed short: %d\n", SHRT_MAX);
    printf("unsigned short: %d\n", USHRT_MAX);
    printf("signed int: %d\n", INT_MAX);
    printf("unsigned int: %d\n", UINT_MAX);
    printf("signed long: %ld\n", LONG_MAX);
    printf("unsigned long: %lu\n", LONG_MAX);
    printf("float: %f\n", FLT_MAX);
    printf("double: %f\n", DBL_MAX);

    printf("\nMinimum values:\n");
    printf("unsigned char: %d\n", CHAR_MIN);
    printf("signed short: %d\n", SHRT_MIN);
    printf("signed int: %d\n", INT_MIN);
    printf("signed long: %ld\n", LONG_MIN);
    printf("unsigned long: %lu\n", LONG_MIN);
    printf("float: %f\n", FLT_MIN);
    printf("double: %f\n", DBL_MIN);
    
    return 0;
}
