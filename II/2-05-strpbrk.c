#include <stdio.h>

#define MAXLINE 1000

enum bool{FALSE, TRUE};

int pointerToFirst(char text[], char searched[]);
int getLine(char line[], int max);

int main()
{
    enum bool exit = FALSE;
    int textLen, searchedLen, pointer;
    char text[MAXLINE], searched[MAXLINE];

    while (!exit) {
        printf("Insert text to search characters in:\n");
        if ((textLen = getLine(text, MAXLINE)) == 0)
            exit = TRUE;
        printf("Insert characters to search for (without blanks in between):\n");
        if ((searchedLen = getLine(searched, MAXLINE)) == 0)
            exit = TRUE;
        
        pointer = pointerToFirst(text, searched);
        if (pointer > -1) 
            printf("Found character: %c at index %d\n\n", text[pointer], pointer);
        else
            printf("Couldn't find any of the searched characters.\n\n");
    }
    return 0;
}

int pointerToFirst(char text[], char searched[])
{
    int index, i, j;

    for (i = 0; text[i] != '\0'; ++i) {
        for (j = 0; searched[j] != '\0'; ++j) {
            if (text[i] == searched[j])
                return i;
        }
    }
    return -1;
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    line[i] = '\0';
    return i;
}
