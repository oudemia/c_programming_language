#include <stdio.h>

#define MAXLINE 1000

enum bool{FALSE, TRUE};

int getLine(char s[], int lim);
int htoi(char s[]);

int main()
{
    int len;
    char line[MAXLINE];
    int decimal;

    while ((len = getLine(line, MAXLINE) > 0)) {
        decimal = htoi(line);

        if (decimal > -1) 
            printf("decimal: %d\n\n", decimal);
        else
            printf("%d Error: input is not a valid hexadecimal number\n\n", decimal);
    } 
    
    return 0;
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    line[i] = '\0';
    return i;
}

int htoi(char s[]) {

    int startIndex = 0;
    int digit;
    int multiplier = 1;
    int decimal = 0;

    if (s[0] == '0') {
        if (s[1] == 'x' || s[1] == 'X')
            startIndex = 2;
        else
            return -1;
    }

    for (int i = startIndex; s[i] != '\0'; ++i) {
        if (s[i] >= '0' && s[i] <= '9') {
            digit = s[i] - '0';
        }
        else if (s[i] >= 'a' && s[i] <= 'f') {
            digit = 10 + (s[i] - 'a');
        }
        else if (s[i] >= 'A' && s[i] <= 'F') {
            digit = 10 + (s[i] - 'A');
        }
        else {
            return -1;
        }

        digit *= multiplier;
        multiplier *= 16;

        decimal += digit;
    }

    return decimal;
}
