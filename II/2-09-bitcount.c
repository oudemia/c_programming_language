/* observation:
 * x &= (x - 1) deletes the rightmost 1-bit, because:
 * 
 * 1. case: x = ***1
 *  then x is equal to (x - 1) in every bit position but position 0,
 *  so x & (x - 1) equals x but in position 0, where it is 0
 *
 * 2. case : x = ***10...0
 *  then (x - 1) = ***01...1, which means that x & (x - 1) is equal to x in
 *  its *** part. Where the first 1-bit in x is positioned, there is a 0 
 *  in (x - 1) and all bits on lower positions are 1, where these in x are
 *  0. Therefore they are all 0-bits in x & (x - 1)
 */

# include <stdio.h>
# include <stdlib.h>

#define MAXLINE 1000

enum bool {FALSE, TRUE};
int getInt(int* number, int max);
int fastBitcount(int x);
int powOfTen(int pow);

int main()
{
    int x, xLen;

    for (;;) {
        printf("Insert number x:\n");
        if ((xLen = getInt(&x, MAXLINE)) <= 0) {
            printf("Error: x has to have an integer value\n");
            exit(0);
        }

        printf("number of 1-bits in x: %u\n\n", fastBitcount(x));

    }
    return 0;
}


int getInt(int* number, int max)
{
    int c, i, j, input[max];

    *number = 0;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    return i;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}


/* return value of x rotated to the right by n positions
 */
int fastBitcount(int x)
{
    int count = 0;
    while ( x != 0 ) {
        x = x & (x - 1);
        count++;
    }
    return count;
}
