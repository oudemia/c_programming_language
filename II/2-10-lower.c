# include <stdio.h>
# include <stdlib.h>
#define MAXLINE 1000


char lower(char c);

int main()
{
    char c;
    while ((c = getchar()) != EOF) {
        putchar(lower(c));
    }
    return 0;
}

char lower(char c)
{
    return (c <= 'Z' && c >= 'A') ? (c - 'A' + 'a') : c;
}

