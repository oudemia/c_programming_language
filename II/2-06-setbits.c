# include <stdio.h>
# include <stdlib.h>

#define MAXLINE 1000

int getInt(int* number, int max);
unsigned setbits(unsigned x, unsigned y, int n, int p);
int powOfTen(int pow);

int main()
{
    int x, y, n, p, xLen, yLen, nLen, pLen;

    for (;;) {
        printf("Insert bit string x:\n");
        if ((xLen = getInt(&x, MAXLINE)) <= 0) {
            printf("Error: please enter a positive number as x\n");
            exit(0);
        }

        printf("Insert bit string y:\n");
        if ((yLen = getInt(&y, MAXLINE)) == 0) {
            printf("Error: please enter a positive number as y\n");
            exit(0);
        }
        printf("How many bits should be taken from x?\n");
        if ((nLen = getInt(&n, MAXLINE)) == 0 || nLen > xLen) {
            printf("Error: input has to be a positive number not greater that the number of bits of x\n");
            exit(0);
        }

        printf("At which position is the first bit to be taken from x?");
        if ((pLen = getInt(&p, MAXLINE)) == 0 || pLen > xLen - nLen) {
            printf("Error: input has to be a positive number not greater that the number of bits of x\n");
            exit(0);
        }

        printf("result: %u\n\n", setbits((unsigned)x, (unsigned)y, n, p));

    }
    return 0;
}


int getInt(int* number, int max)
{
    int c, i, j, input[max];

    *number = 0;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    return i;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}


/* return x with the n bits that begin at position p set to the rightmost 
 * n bits of y, leaving the other bits unchanged
 */
unsigned setbits(unsigned x, unsigned y, int n, int p)
{
    return x & ~(~(~0 << n) << (p+1-n)) | ( y & (~(~0<<n)) << (p+1-n));
}
