#include <stdio.h>

#define MAXLINE 1000

enum bool{FALSE, TRUE};

void squeeze(char text[], char blacklist[]);
int getLine(char line[], int max);

int main()
{
    enum bool exit = FALSE;
    int textLen, blacklistLen;
    char text[MAXLINE], blacklist[MAXLINE], result[MAXLINE];

    while (!exit) {
        printf("Insert text to delete characters from:\n");
        if ((textLen = getLine(text, MAXLINE)) == 0)
            exit = TRUE;
        printf("Insert characters to delete (without blanks in between):\n");
        if ((blacklistLen = getLine(blacklist, MAXLINE)) == 0)
            exit = TRUE;
        
        squeeze(text, blacklist);
        printf("result: %s\n\n", text);
    }
    return 0;
}

void squeeze(char text[], char blacklist[])
{
    int i, j, k;
    enum bool onBlacklist;

    for (i = j = 0; text[i] != '\0'; i++) {
        onBlacklist = FALSE;
        for (k = 0; blacklist[k] != '\0'; ++k) {
            if (text[i] == blacklist[k])
                onBlacklist = TRUE;
        }
        if (!onBlacklist) {
            text[j++] = text[i]; 
        }
    }
    text[j] = '\0';
}

int getLine(char line[], int max)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1)
            line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        i++;
    }
    line[i] = '\0';
    return i;
}
