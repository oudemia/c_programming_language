# include <stdio.h>
# include <stdlib.h>

#define MAXLINE 1000

int getInt(int* number, int max);
unsigned invert(unsigned x, int n, int p);
int powOfTen(int pow);

int main()
{
    int x, n, p, xLen, yLen, nLen, pLen;

    for (;;) {
        printf("Insert number x:\n");
        if ((xLen = getInt(&x, MAXLINE)) <= 0) {
            printf("Error: please enter a positive number as x\n");
            exit(0);
        }

        printf("How many bits of x should be inverted?\n");
        if ((nLen = getInt(&n, MAXLINE)) == 0 || nLen > xLen) {
            printf("Error: input has to be a positive number not greater that the number of bits of x\n");
            exit(0);
        }

        printf("At which position is the first bit to be taken from x?\n");
        if ((pLen = getInt(&p, MAXLINE)) == 0 || pLen > nLen) {
            printf("Error: input has to be a positive number not greater that the number of bits of x\n");
            exit(0);
        }

        printf("result: %u\n\n", invert((unsigned)x, n, p));

    }
    return 0;
}


int getInt(int* number, int max)
{
    int c, i, j, input[max];

    *number = 0;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    return i;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}


/* return x with the n bits that begin at position p inverted
 */
unsigned invert(unsigned x, int n, int p)
{
    // unsigned inverted = ((~(x >> p) & ~(~0 << n))) << p;
    
    // unsigned mask =  (~0 << (p + n + 1)) || ~(~0 << (p - 1));
    
    // unsigned cut_out_x = x & mask;
    
    unsigned mask = ~(~0 << n) << (p + 1 - n);

    return x ^ mask;
}
