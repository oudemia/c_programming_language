/* for (i = 0; i < lim - 1 && (c = getchar()) != '\n' && c != EOF; ++i)
 * s[i] = c;
 */

#include <stdio.h>

#define MAXLINE 1000

int getLine(char s[], int lim);
void printLine(char s[], int end);

enum bool{FALSE, TRUE};

int main()
{
    int len;
    char line[MAXLINE];

    while ((len = getLine(line, MAXLINE)) > 0) {
        printf("%s\n", line);

    }

    return 0;
}

int getLine(char s[], int lim)
{
    int c;
    int i = 0;

    enum bool inLine = TRUE;

    while (inLine == TRUE) {
        if (i >= lim - 1) 
            inLine = FALSE;
        
        c = getchar();
        if (c == '\n')
            inLine = FALSE;
        if (c == EOF)
            inLine = FALSE;

        if (inLine == TRUE) {
            s[i] = c;
            i++;
        }
    }

    if (c == '\n') {
        s[i] = c;
        ++i;
    }

    s[i] = '\0';
    return i;
}
