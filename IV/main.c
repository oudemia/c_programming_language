# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include "polishRevCalc.h"

# define MAX_OP 100 // max size of operator or operand
# define VARIABLE_COUNT 26 // single letter upper case names

enum bool {FALSE, TRUE};
int getop(char []);
void push(double);
double pop(void);

double variables[VARIABLE_COUNT] = { 0 };
enum bool wasAssigned[VARIABLE_COUNT]; // to print former value
double lastPrinted; // most recently printed value

int main()
{
    int type, opOneInt, opTwoInt;
    double opOne, opTwo, varVal, lastPrinted;
    char op[MAX_OP], varName;
    enum bool nothingPushed = FALSE;

    for (int i = 0; i < VARIABLE_COUNT; i++) {
        wasAssigned[i] = FALSE;
    }

    while((type = getop(op)) != EOF) {
        switch(type) {
            case NUMBER:
                push(atof(op));
                break;
            case '+':
                push(pop() + pop());
                nothingPushed = FALSE;
                break;
            case '-':
                opTwo = pop();
                push(pop() - opTwo);
                nothingPushed = FALSE;
                break;
            case '*':
                push(pop() * pop());
                nothingPushed = FALSE;
                break;
            case '/':
                opTwo = pop();
                if (opTwo != 0.0) {
                    push(pop() / opTwo);
                    nothingPushed = FALSE;
                }
                else
                    printf("\tError: division by zero\n");
                break;
            case '%':
                opTwo = pop();
                opTwoInt = (int) opTwo;
                opOne = pop();
                opOneInt = (int) opOne;
                if (opTwo != 0.0) {
                    push(opOneInt % opTwoInt);
                    nothingPushed = FALSE;
                }
                break;
            case '=':
                varVal = pop();
                pop(); // tos is value of var, not var name
                if (varName >= 'A' && varName <= 'Z') {
                    if (wasAssigned[varName - 'A'] == TRUE)
                        printf("\toverwriting former value %.8g with %.8g\n", 
                                variables[varName - 'A'], varVal);
                    else
                        printf("\t%c = %.8g\n", varName, varVal);
                    variables[varName - 'A'] = varVal;
                    wasAssigned[varName - 'A'] = TRUE;
                }
                else {
                    printf("\tError: invalid variable name: %c\n", varName);
                }
                nothingPushed = TRUE;
                break;
            case '\n':
                if (!nothingPushed) {
                    lastPrinted = pop();
                    printf("\tresult: %.8g\n", lastPrinted);
                }
                nothingPushed = FALSE;
                break;
            case 't': // t  - top element of stack
                printf("\ttop of stack: %.8g\n", peek());
                break;
            case 'd': // d - duplicate
                duplicateTop();
                printf("\tduplicated top element of stack\n");
                break;
            case 'x': // x - swap
                swapTwoTop();
                printf("\tswapped top two stack elements\n");
                break;
            case 'f': // f - flush / clear
                clear();
                printf("\tcleared the stack\n");
                nothingPushed = TRUE;
                break;
            case 's': // s - sin
                push(sin(pop()));
                nothingPushed = FALSE;
                break;
            case 'c': // c - cos
                push(cos(pop()));
                nothingPushed = FALSE;
                break;
            case 'l': // l - log 
                push(log(pop()));
                nothingPushed = FALSE;
                break;
            case 'e': // e - exp
                push(exp(pop()));
                nothingPushed = FALSE;
                break;
            case 'p': // p - pow
                opTwo = pop();
                opOne = pop();
                push(pow(opOne, opTwo));
                nothingPushed = FALSE;
                break;
            case 'r': // last result printed
                printf("\tmost recent result: %.8g\n", lastPrinted);
                nothingPushed = TRUE;
                break;
            default:
                if (type >= 'A' && type <= 'Z') {
                    varName = type;
                    push(variables[type - 'A']);
                    nothingPushed = FALSE;
                }
                else
                    printf("\tError: unknown command %s\n", op);
                break;
        }
    }
    return 0;
}
