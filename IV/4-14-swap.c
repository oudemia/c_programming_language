# include <stdio.h>
# include <stdlib.h>

# define MAXLINE 1000

# define swap(t, x, y) t temp = x; x = y; y = temp;

int getInt(int *number, int max);
int powOfTen(int pow);
int stringLen(char s[]);

int main()
{
    int xval, yval;

    printf("Enter a number x:\n");
    if((getInt(&xval, MAXLINE)) == -1) {
        printf("Error: invalid input\n");
        exit(-1);
    }
    printf("Enter a number y:\n");
    if((getInt(&yval, MAXLINE)) == -1) {
        printf("Error: invalid input\n");
        exit(-1);
    }

    swap(int, xval, yval);

    printf("\t x = %d\ty = %d\tmagic!\n", xval, yval);

    return 0;
}

int getInt(int* number, int max)
{
    int c, i, j, sign, input[max];

    *number = 0;
    i = 0;

    if ((c = getchar()) == '-')
        sign = -1;
    else if (c >= '0' && c <= '9') {
        input[i] = c - '0';
        i++;
        sign = 1;
    }
    else
        return -1;

    for (; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    *number *= sign;

    return 0;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}

