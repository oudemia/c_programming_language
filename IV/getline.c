# include <stdio.h>
int getLine(char line[], int max);

int getLine(char s[], int max)
{
    int c, i = 0;

    while (--max > 0 && (c = getchar()) != EOF && c != '\n') {
        s[i++] = c;
    }
    if (c == '\n') {
        s[i++] = c;
    }
    s[i] = '\0';
    
    return i; // position of the last character in s
}
