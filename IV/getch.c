# include <stdio.h>

# define BUF_SIZE 100

/* suppose that there is ony pushback of one character maximum:
 * we turn buf into a single char and initialize it as '0'
 * getch returns buf, if not '0' and resets it
 * ungetch sets buf to value, if it is '0'
 */

/* a char is nothing more than an int...
 * if we turn the buffer into an int array, it can handle EOF,
 * which has a value of -1
 */
static int buf[BUF_SIZE];
static int bufp = 0;

int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}


void ungetch(int c)
{
    if (bufp >= BUF_SIZE)
        printf("Error: getch buffer overflow\n");
    else
        buf[bufp++] = c;
}

// does not have to deal with bufp and BUF_SIZE, ungetch does that already
void ungets(char s[])
{
    for (int i = 0; s[i] != '\0'; i++)
        ungetch(s[i]);
}
