# include <stdio.h>
# include "polishRevCalc.h"

# define MAX_VAL 100 // max depth of val stack

int sp = 0;
double val[MAX_VAL];

void push(double f)
{
    if (sp < MAX_VAL)
        val[sp++] = f;
    else
        printf("Error: can't push %g onto full stack\n", f);
}


double pop(void)
{
    if (sp > 0)
        return val[--sp];
    else {
        printf("Error: can't pop from empty stack\n");
        return 0.0;
    }
}


double peek(void)
{
    if (sp >= 0)
        return val[sp];
    else {
        printf("Error: empty stack\n");
        return 0.0;
    }
}


void duplicateTop(void)
{
    push(peek());
}


void swapTwoTop(void)
{
    double first = pop();
    double second = pop();
    push(first);
    push(second);
}


void clear(void)
{
    sp = 0;
}
