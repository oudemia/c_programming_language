# include <stdio.h>
# include <stdlib.h>
# include "getline.c"

# define MAX_LINE 1000

double atofSci(char s[]);
double powOfTen(int pow);

int main()
{
    char input[MAX_LINE];
    double val;

    printf("Enter a floating point number:\n");
    if (getLine(input, MAX_LINE) < 0) {
        printf("Error: invalid input\n");
        exit(0);
    }

    val = atofSci(input);
    printf("\nresult: %f\n", val);

    return 0;
}


double atofSci(char s[])
{
    double val, pow, exp;
    int i, sign, expSign;

    for (i = 0; isspace(s[i]); i++) ;
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '+')
        i++;
    
    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');

    if (s[i] == '.')
        i++;

    for (pow = 1.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
        pow *= 10;
    }

    if (s[i] == 'e' || s[i] == 'E')
        i++;

    expSign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-')
        i++;

    for (exp = 0; isdigit(s[i]); i++) {
        exp = 10 * exp + (s[i] - '0');
    }
    exp *= expSign;

    printf("\nexp: %f, powOfTen: %f\n", exp, powOfTen(exp));

    return (sign * val / pow) * powOfTen(exp);
}

double powOfTen(int pow)
{
    double val = 1.0;

    double fac = (pow < 0) ? 0.1 : 10.0;
    pow = (pow < 0) ? -pow : pow;
    
    while (pow > 0) {
        val = fac * val;
        pow--;
    }
    return val;
}
