# include <stdio.h>
# include <stdlib.h>

# define MAX_LINE 1000

int getLine(char line[], int max);
int strrindex(char s[], char t[]);

int main()
{
    char s[MAX_LINE], t[MAX_LINE], pattern[MAX_LINE];
    int i, found = -1;

    printf("Enter the pattern to search for (max. one line):\n");
    while (getLine(t, MAX_LINE) <= 0) {
        printf("Error: please provide a non empty character string\n");
    }

    printf("Enter the text to search the pattern in:\n");
    while (getLine(s, MAX_LINE) > 0 && found == -1) {
        if ((found = strrindex(s, t)) >= 0) {
            for (i = 0; t[i] != '\n'; i++)
                pattern[i] = t[i];
            pattern[i] = '\0';
            printf("\n\nFound ’%s’ at position %d.\n", pattern, found);
            exit(0);
        }
    }

    return 0;
}

int getLine(char s[], int max)
{
    int c, i = 0;

    while (--max > 0 && (c = getchar()) != EOF && c != '\n') {
        s[i++] = c;
    }
    if (c == '\n') {
        s[i++] = c;
    }
    s[i] = '\0';
    
    return i; // position of the last character in s
}

// returns position of the rightmost occurrence of t in s or -1
int strrindex(char s[], char t[])
{
    int i, j, k, lastS, lastT;

    for (lastS = 0; s[lastS] != '\0'; ++lastS) ;
    for (lastT = 0; t[lastT] != '\0'; ++lastT) ;
    lastS--; 
    lastT-= 2; // skip '\n' as well as '\0'

    for (i = lastS; i >= 0; i--) {
        for (j = i, k = lastT; k >= 0 && s[j] == t[k]; j--, k--) ;
        if (k == -1) {
            return i;
        }
    }
    return -1;
}
