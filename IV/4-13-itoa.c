# include <stdio.h>
# include <stdlib.h>

# define MAXLINE 1000

/* the book version itoa doesn't handle the largest negative number because
 * |max| = |min| - 1, so the following is not possible: n = -n
 */

int getInt(int *number, int max);
int powOfTen(int pow);
void itoa(int n, char s[]);
int abs(int num);
void reverse(char s[]);
void revStep(char s[], int left, int right);
int stringLen(char s[]);

int main()
{
    int n;
    char s[MAXLINE];

    printf("Enter the number to convert:\n");
    if((getInt(&n, MAXLINE)) == -1) {
        printf("Error: invalid input\n");
        exit(-1);
    }

    itoa(n, s);
    printf("\n\nresult string:\n%s\n", s);

    return 0;
}


// convert n to characters in s
void itoa(int n, char s[])
{
    int nextDigit, sign = n, j, i = 0;

    do {
        nextDigit = n % 10;
        s[i++] = abs(nextDigit) + '0'; // now abs produces number < max_int
    } while ((n /= 10) != 0);

    if (sign < 0)
        s[i++] = '-';
   s[i] = '\0';
    reverse(s);
}


int getInt(int* number, int max)
{
    int c, i, j, sign, input[max];

    *number = 0;
    i = 0;

    if ((c = getchar()) == '-')
        sign = -1;
    else if (c >= '0' && c <= '9') {
        input[i] = c - '0';
        i++;
        sign = 1;
    }
    else
        return -1;

    for (; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    *number *= sign;

    return 0;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}

int abs(int num)
{
    return num < 0 ? num * (-1) : num;
}


void reverse(char s[])
{
    int len = stringLen(s);
    revStep(s, 0, len - 1); // leave out '\0' at the end
}


void revStep(char s[], int left, int right)
{
    static int temp;
    temp = s[left];
    s[left] = s[right];
    s[right] = temp;

    if (left < right)
        revStep(s, ++left, --right);

}


int stringLen(char s[])
{
    int i = 0;
    for ( ; s[i] != '\0'; i++)
        ;
    return i;
}
