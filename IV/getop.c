# include <stdio.h>
# include <stdlib.h>
# include <ctype.h>
# include "polishRevCalc.h"

int getch(void);
void ungetch(int);

/* an alternate version of getop that uses getline instead of getch and
 * ungetch would define an input string using getline and then use an 
 * index to navigate through the input
 */
int getop(char s[])
{
    int c, i, sign, next, cIsSign;
    static int lastC = 0; // instead of ungetch(...)

    if (lastC == 0)
        c = getch();
    else {
        c = lastC;
        lastC = 0;
    }
    // lastC now is 0 in any case

    while ((s[0] = c) == ' ' || c == '\t')
        c = getch(); // skip whitespace
    s[1] = '\0';
    
    if (c == '\n') {
        return c;
    }

    next = getch();
    cIsSign = (c == '+' || c == '-') && (isdigit(next) || next == '.');
    lastC = next;
    
    i = 0;

    if (cIsSign) {
        if (lastC == 0)
            c = getch();
        else {
            c = lastC;
            lastC = 0;
        }
        s[++i] = c;
    }
    else if ((!isdigit(c) && c != '.')) {// NaN
        return c;
    }
    if (isdigit(c)) {
        if (lastC == 0)
            c = getch();
        else {
            c = lastC;
            lastC = 0;
        }
        while (isdigit(s[++i] = c)) // collect integer part
            c = getch();
    }
    if (c == '.')
        while (isdigit(s[++i] = c = getch())) // collect fraction part
            ;

    s[i] = '\0';
    if (c != EOF)
        lastC = c;

    return NUMBER;
}
