# define NUMBER '0' // signal that number was found

void push(double);
double pop(void);
int getop(char []);
int getch(void);
void ungetch(int);
void ungets(char []);
double peek(void);
void duplicateTop(void);
void swapTwoTop(void);
void clear(void);
