# include <stdio.h>
# include <stdlib.h>
# include "util.h"

# define debug(expr, t) printf( "\t" #expr " = %" #t "\n", expr)

void pstrncat(char *s, char *t, int n);
void pstrncpy(char *s, char *t, int n);
void pstrcpy(char *s, char *t);
int pstrncmp(char *s, char *t, int n);

int main()
{
    int n, comp;
    char s[MAX_LEN], t[MAX_LEN], backup_s[MAX_LEN], backup_t[MAX_LEN], res;

    printf("\tEnter the first character string:\n");
    if (getLine(backup_s, MAX_LEN) <= 0) {
        printf("\tError: invalid input\n");
        exit(0);
    }

    printf("\tEnter the second character string:\n");
    if (getLine(backup_t, MAX_LEN) <= 0) {
        printf("\tError: invalid input\n");
        exit(0);
    }

    printf("\tEnter parameter n:\n");
    if (getint(&n) < 0) {
        printf("\tError: please enter an integer value\n");
        exit(0);
    }

    pstrcpy(s, backup_s);
    pstrcpy(t, backup_t);
    pstrncat(s, t, n);
    printf("\n\tConcatenation result:\n\n%s\n", s);

    pstrcpy(s, backup_s);
    pstrcpy(t, backup_t);
    pstrncpy(s, t, n);
    printf("\n\tCopy result:\n\n%s\n", s);

    pstrcpy(s, backup_s);
    pstrcpy(t, backup_t);
    if ((comp = pstrncmp(s, t, n)) < 0)
        res = '<';
    else if (comp == 0)
        res = '=';
    else
        res = '>';

    printf("\n\tComparison result: first %c second\n", res);

    return 0; 
}


// strncat with pointers
void pstrncat(char *s, char *t, int n)
{
    while (*s++) // find end of s
        ;
    s--;
    while ((*s++ = *t++) && --n > 0) // copy t to s
        ;
}

void pstrcpy(char *s, char *t)
{
    while ((*s++ = *t++))
        ;
}

void pstrncpy(char *s, char *t, int n)
{
    while ((*s++ = *t++) && --n > 0)
        ;
    *s = '\0';
}

int pstrncmp(char *s, char *t, int n)
{
    for ( ; *s == *t && n > 1; s++, t++, n--)
        if (*s == '\0')
            return 0;

    return *s - *t;

}
