# include <stdio.h>
#include "util.h"

# define debug(expr, t) printf( "\t" #expr " = %" #t "\n", expr)
# define MAX_SIZE 100

int main()
{
    int n, i, getfloat(float *);
    float floatArray[MAX_SIZE];

    printf("\tEnter floating point values to collect:\n");
    for (n = 0; n < MAX_SIZE && getfloat(&floatArray[n]) != EOF; n++)
        ;

    printf("\n\tCollected values:\n");
    for (i = 0; i <= n; i++)
        printf("\t%f\n", floatArray[i]);

    return 0; 
}
