# include <stdio.h>
# include <string.h>
# include <time.h>
# include "alloc.h"

# define MAXLINES 5000 // max amount of lines
# define MAXLEN 1000 // max lenght of a line

char *lineptr[MAXLINES];

int readlines(char *lineptr[], int nlines);
void writelines(char *lineptr[], int nlines);

void qsort(char *lineptr[], int left, int right);

int getLine(char *, int);


int main()
{
    int nlines;
    clock_t timer;

    printf("\tPlease enter the lines to sort:\n");
    if ((nlines = readlines(lineptr, MAXLINES)) >= 0) {
        timer = clock();
        qsort(lineptr, 0, nlines - 1);
        timer = clock() - timer;
        double runtime = ((double)timer)/CLOCKS_PER_SEC;
        printf("\n\tSorted result:\n");
        writelines(lineptr, nlines);
        printf("\n\tThe book version took %f seconds to execute.\n\n", runtime);
        return 0;
    }
    printf("\tError; input too large to sort\n");
    return 1;
}


int readlines(char *lineptr[], int maxlines)
{
    int len, nlines;
    char *p, line[MAXLEN];

    nlines = 0;
    while ((len = getLine(line, MAXLEN)) > 0)
        if (nlines >= maxlines || (p = alloc(len)) == NULL)
            return -1;
        else {
            line[len - 1] = '\0'; // delete '\n'
            strcpy(p, line);
            lineptr[nlines++] = p;
        }
    return nlines;
}


void writelines(char *lineptr[], int nlines)
{
    while (nlines--)
        printf("%s\n", *lineptr++);
}


void qsort(char *v[], int left, int right)
{
    int i, last;
    void swap(char *v[], int i, int j);

    if (left >= right)
        return;

    swap(v, left, (left + right) / 2);
    last = left;

    for (i = left + 1; i <= right; i++)
        if (strcmp(v[i], v[left]) < 0)
            swap(v, ++last, i);

    swap(v, left, last);
    qsort(v, left, last-1);
    qsort(v, last+1, right);
}

void swap(char *v[], int i, int j)
{
    char *temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}


int getLine(char s[], int max)
{
    int c, i = 0;

    while (--max > 0 && (c = getchar()) != EOF && c != '\n') {
        s[i++] = c;
    }

    if (c == '\n') {
        s[i] = c;
        i++;
    }
    s[i] = '\0'; // this version does not append a final '\n'
    
    return i; // position of the last character in s
}

