# include <stdio.h>
# include <string.h>
# include <time.h>
# include "alloc.h"
# include "util.h"

# define MAXLINES 5000 // max amount of lines
# define MAXLEN 1000 // max lenght of a line

static char daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

int dayOfYear(int year, int month, int day);
void monthDay(int year, int yearday, int *pmonth, int *pday);
int isLeap(int year);

int main()
{
    int year, month, day, day_of_year, check_day, check_month;
    clock_t timer;

    printf("\tPlease enter a year:\n");
    if (getint(&year) < 0) {
        printf("\tError: please enter an integer value\n");
        return 1;
    }

    printf("\tPlease enter a month (number):\n");
    if (getint(&month) < 0) {
        printf("\tError: please enter an integer value\n");
        return 1;
    }

    printf("\tPlease enter a day of the month (number):\n");
    if (getint(&day) < 0) {
        printf("\tError: please enter an integer value\n");
        return 1;
    }

    if ((day_of_year = dayOfYear(year, month, day)) <= 0) {
        printf("\tError; invalid date.\n");
        return 1;
    }

    monthDay(year, day_of_year, &check_month, &check_day);

    printf("\tThe given day was day number %d of %d\n", day_of_year, year);
    printf("\tChecking the result: you entered the %d. day of the %d. month of %d.\n",
            check_day, check_month, year);
    return 0;
}


// set day of year from month and day
int dayOfYear(int year, int month, int day)
{
    int i, leap = isLeap(year);

    if (year < 0 || month < 1 || month > 12 || day < 1 || day > *(*(daytab + leap) + month))
        return -1;

    for(i = 1; i < month; i++)
        day += *(*(daytab + leap)+i);

    return day;
}


// set month and day from day of year
void monthDay(int year, int yearday, int *pmonth, int *pday)
{
    int i, leap = isLeap(year);

    int total_days = leap ? 366 : 365;
    if (year < 0 || yearday < 1 || yearday > total_days)
        return;
    
    for (i = 1; yearday > *(*(daytab + leap)+i); i++)
        yearday -= *(*(daytab + leap)+i);

    *pmonth = i;
    *pday = yearday;
}

int isLeap(int year)
{
    return year%4 == 0 && year%100 != 0 || year %400 == 0;
}
