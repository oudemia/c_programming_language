# include <ctype.h>
# include <stdio.h>
# include "util.h"

static int issign(int c);

int getint(int *pn)
{
    int c, sign;

    while (isspace(c = getch())) // skip whitespace
        ;
    if (!isdigit(c) && c != EOF && !issign(c)) {
        ungetch(c); // NaN
        return 0;
    }

    sign = (c == '-') ? -1 : 1;
    if(issign(c)) {
        c = getch();
        if (!isdigit(c)) {
            ungetch(c);
            return 0;
        }
    }

    for (*pn = 0; isdigit(c); c = getch())
        *pn = 10 * *pn + (c - '0');

    *pn *= sign;

    if(c != EOF)
        ungetch(c);

    return c;
}


int getfloat(float *pn)
{
    int c;
    float sign, nextDigit;

    while (isspace(c = getch())) // skip whitespace
        ;
    if (!isdigit(c) && c != EOF && !issign(c) && c != '.') {
        ungetch(c); // NaN
        return 0;
    }

    sign = (c == '-') ? -1.0 : 1.0;
    if(issign(c)) {
        c = getch();
        if (!isdigit(c)) {
            ungetch(c);
            return 0;
        }
    }

    for (*pn = 0; isdigit(c); c = getch()) // collect integer part
        *pn = 10 * *pn + (c - '0');

    if (c == '.') { // collect fraction part
        c = getch();
        for (int powOfTen = 10; isdigit(c); powOfTen *= 10) {
            nextDigit = (float)(c - '0') / powOfTen;
            *pn = *pn + nextDigit;
            c = getch();
        }
    }


    *pn *= sign;

    if(c != EOF)
        ungetch(c);

    return c;
}


static int issign(int c)
{
    return (c == '+' || c == '-');
}
