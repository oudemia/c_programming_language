# include <stdio.h>
# include <stdlib.h>
# include "util.h"

# define debug(expr, t) printf( "\t" #expr " = %" #t "\n", expr)

void pstrcat(char *s, char *t);

int main()
{
    char s[MAX_LEN], t[MAX_LEN];

    printf("\tEnter the character string you want to append to:\n");
    if (getLine(s, MAX_LEN) <= 0) {
        printf("\tError: invalid input\n");
        exit(0);
    }

    printf("\tEnter the character string you want to append:\n");
    if (getLine(t, MAX_LEN) <= 0) {
        printf("\tError: invalid input\n");
        exit(0);
    }

    pstrcat(s, t);

    printf("\n\tResulting character string:\n\n%s\n", s);

    return 0; 
}


// strcat with pointers
void pstrcat(char *s, char *t)
{
    while (*s++) // find end of s
        ;
    s--;
    while ((*s++ = *t++)) // copy t to s
        ;
}
