# include <stdio.h>
# include <stdlib.h>
# include "util.h"

# define debug(expr, t) printf( "\t" #expr " = %" #t "\n", expr)

int strend(char *s, char *t);

int main()
{
    char s[MAX_LEN], t[MAX_LEN];

    printf("\tEnter the character string you want to append to:\n");
    if (getLine(s, MAX_LEN) <= 0) {
        printf("\tError: invalid input\n");
        exit(0);
    }

    printf("\tEnter the character string you want to append:\n");
    if (getLine(t, MAX_LEN) <= 0) {
        printf("\tError: invalid input\n");
        exit(0);
    }

    int bool = strend(s, t);

    if (bool)
        printf("\tThe end of first string contains the second string\n");
    else
        printf("\tThe end of first string doesn't contain the second string\n");

    return 0; 
}


// strcat with pointers
int strend(char *s, char *t)
{
    int tLen = 0;
    while (*s++) // go to end of s
        ;
    while (*t++) // go to end of t
        tLen++;

    while (tLen-- >= 0) // compare
        if (*s != *t)
            return 0;

    return 1;
}
