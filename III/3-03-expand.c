# include <stdio.h>
# include <stdlib.h>

# define MAX_SIZE 100000

int expand(char s[], char t[]);
int getInput(char s[], int size);
int isRange(char first, char second);
int isNumerical(char c);
int isLowerCase(char c);
int isUpperCase(char c);


int main()
{
    char s[MAX_SIZE], t[MAX_SIZE];
    printf("Insert text:\n");

    if(getInput(s, MAX_SIZE) == -1) {
        printf("Error: invalid input/ too many characters\n");
        exit(-1);
    }

    printf("hi");

    if (expand(s, t) == -1) {
        printf("Error: couldn't expand shorthand notations\n");
        exit(-1);
    }
    printf("\n\nresulting text:\n%s\n", t);

    return 0;
}


int getInput(char s[], int size)
{
    int i, c;
    for (i = 0; (c = getchar()) != EOF && i < size - 1; ++i) {
        s[i] = c;
    }

    if (i >= size - 1)
        return -1;

    ++i;
    s[i] = '\0';
    return 0;
}

int expand(char s[], char t[])
{
    int i, j, k, lastC, c, nextC;
    
    t[0] = s[0];
    lastC = s[0];
    j = 1;
    for (i = 1; (c = s[i]) != '\0'; ++i) {
        switch (c) {
            case '-':
                nextC = s[i+1];
                if (isRange(lastC, nextC)) {
                    for (k = 1; (lastC + k) < nextC; ++k) {
                        t[j++] = lastC + k;
                    }
                }
                else
                    t[j++] = c;
                break;
            default:
                t[j++] = c;
                break;
        }
        lastC = c;
    }
    return 0;
}


int isRange(char first, char second)
{
    return ((isNumerical(first) && isNumerical(second)) || 
            (isLowerCase(first) && isLowerCase(second)) ||
            (isUpperCase(first) && isUpperCase(second))) &&
        first < second;
}


int isNumerical(char c)
{
    return c >= '0' && c <= '9';
}


int isLowerCase(char c)
{
    return c >= 'a' && c <= 'z';
}


int isUpperCase(char c)
{
    return c >= 'A' && c <= 'Z';
}
