# include <stdio.h>
# include <stdlib.h>

# define MAX_INPT 100000

int getInput(char s[], int size);
void escape(char s[], char t[]);
void reverseEscape(char t[], char u[]);

int main()
{
    char s[MAX_INPT], t[MAX_INPT], u[MAX_INPT];
    printf("Insert a text with invisible characters (max %d):\n", 
            MAX_INPT);
    
    if(getInput(s, MAX_INPT) == -1) {
        printf("Error: invalid input\n");
        exit(-1);
    }

    escape(s, t);
    printf("\n\nresult:\n%s\n\n", t);

    reverseEscape(t, u);
    printf("reverting:\n%s\n", u);
    return 0;
}


int getInput(char s[], int size)
{
    int i, c;
    for (i = 0; (c = getchar()) != EOF && i < size - 1; ++i) {
        s[i] = c;
    }

    if (i >= size - 1)
        return -1;

    ++i;
    s[i] = '\0';
    return 0;
}


void escape(char s[], char t[])
{
    int i, j, c;
    j = 0;

    for (i = 0; (c = s[i]) != '\0'; ++i) {
        switch (c) {
            case '\n':
                t[j++] = '\\';
                t[j++] = 'n';
                break;
            case '\t':
                t[j++] = '\\';
                t[j++] = 't';
                break;
            default:
                t[j++] = c;
                break;
        }
        if (j >= MAX_INPT) {
            printf("Error: out of buffer\n");
            exit(-1);
        }
    }
    t[j] = '\0';
}


void reverseEscape(char t[], char u[])
{

    int i, j, lastC, c;
    
    j = 0;
    lastC = t[0];
    for (i = 1; (c = t[i]) != '\0'; ++i) {
        switch (c) {
            case '\\':
                break;
            case 'n':
                if (lastC == '\\') {
                    u[j++] = '\n';
                }
                break;
            case 't':
                if (lastC == '\\') {
                    u[j++] = '\t';
                }
                break;
            default:
                u[j++] = c;
                break;
        }
        lastC = c;
    }
    if (lastC == '\\')
        u[j++] = '\\';

    u[j] = '\0';
}
