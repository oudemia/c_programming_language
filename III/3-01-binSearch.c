# include <stdio.h>
# include <time.h>
# include <stdlib.h>

# define MIN_CONST 100
# define MAX_CONST 1000
#define MAXLINE 1000

int bookBinSearch(int x, int *v, int n);
int binSearch(int x, int *v, int n);
void fillRandomArray(int *array, int size);
int getInt(int* number, int max);
int powOfTen(int pow);
int intCompar (const void *a, const void *b);

int main()
{
    int x, xIsNum, n, bookIndex, exerIndex;
    srand(time(NULL));
    n = rand() % (MAX_CONST - MIN_CONST) + MIN_CONST;

    int v[n];
    fillRandomArray(v, n);

    printf("Please enter the number to search for:\n");
        if ((xIsNum = getInt(&x, MAXLINE)) != 0) {
            exit(0);
        }
    printf("x: %d\n", x);

    clock_t bookClock, exerClock;
    bookClock = clock();
    bookIndex = bookBinSearch(x, v, n);
    bookClock = clock() - bookClock;
    double bookRuntime = ((double)bookClock)/CLOCKS_PER_SEC;
    if (bookIndex == -1)
        printf("The given number could not be found.\n");
    else
        printf("The given number was found at index %d.\n", bookIndex);
    printf("The book version took %f seconds to execute.\n\n", bookRuntime);

    exerClock = clock();
    exerIndex = binSearch(x, v, n);
    exerClock = clock() - exerClock;
    double exerRuntime = ((double)exerClock)/CLOCKS_PER_SEC;
    if (exerIndex == -1)
        printf("The given number could not be found.\n");
    else
        printf("The given number was found at index %d.\n", exerIndex);
    printf("The book version took %f seconds to execute.\n", exerRuntime);

   return 0;
}


// binary search, book version
int bookBinSearch(int x, int *v, int n)
{
    int low, high, mid;

    low = 0;
    high = n - 1;

    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid])
            high = mid - 1;
        else if (x > v[mid])
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}

// binary search with only one test inside loop
int binSearch(int x, int *v, int n)
{
    int low, high, mid;

    low = 0;
    high = n - 1;

    while (low < high) {
        mid = (low + high) / 2;
        if (x <= v[mid])
            high = mid;
        else
            low = mid + 1;
    }
    return x == v[low] ? low : -1;
}


void fillRandomArray(int *array, int size)
{
    srand(time(NULL));
    for (int i = 0; i < size; ++i) {
        int random = rand();
        array[i] = random % (MAX_CONST - MIN_CONST) + MIN_CONST;
    }
    qsort(array, size, sizeof(int), intCompar);
}


int getInt(int* number, int max)
{
    int c, i, j, input[max];

    *number = 0;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < max - 1 && c >= '0' && c <= '9') {
            input[i] = c - '0';
        }
        else {
            return -1;
        }
    }

    for (j = i - 1; j >= 0; --j) {
        *number += input[j] * powOfTen(i - 1 - j);
    }

    return 0;
}

int powOfTen(int pow) {
    int result = 1;
    for (int i = 0; i < pow; ++i) {
        result *= 10;
    }
    return result;
}

int intCompar(const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}
